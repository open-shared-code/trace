/*
 * loop_test.cpp
 *
 *  Created on: Jul 29, 2018
 *      Author: rmerriam
 */
#include <climits>
#include <tut/tut.hpp>

#include "Trace.h"

//--------------------------------------------------------------------------------------------------------------------------
struct trace_test_data_c {
    std::ostringstream str_out;

    std::osyncstream sync_out { str_out };

    mys::Trace tstrm { sync_out, 'S' };

    //--------------------------------------------------------------------------------------------------------------------------
    std::string test_output() {
        tstrm.flush();
        sync_out.emit();
        return str_out.rdbuf()->str().substr(22);
    }
};
//=====================================================================================================================
namespace tut {

    typedef test_group<trace_test_data_c> tg;
    tg lp_name("Trace Output Containers");

    typedef tg::object trace_test;
    //--------------------------------------------------------------------------------------------------------------------------
    // std::array - char

    template <>
    template <>
    void trace_test::test<1>() {
        set_test_name("std::array signed char / int8_t output");
        std::array test_data { 'A', 'B', 'C', 'D', 'E', };
        std::string td { "ABCDE" };

        tstrm << test_data;
        mys::terr << code_line << test_output() << mys::nl;
        ensure_equals("std::array char[] trace failed", test_output(), td);
    }
    //--------------------------------------------------------------------------------------------------------------------------
    // std::array uint8_t

    template <>
    template <>
    void trace_test::test<2>() {
        set_test_name("std::array unsigned char / uint8_t output");
        std::array<uint8_t, 5> test_data { 0x51, 0x52, 0x48, 0x01, 0x23 };
        std::string td { " 0x51 0x52 0x48 0x01 0x23" };

        tstrm << std::hex << std::setw(4) << std::showbase << std::internal << std::setfill('0') << test_data;
        mys::terr << code_line << std::setw(4) << test_output() << mys::nl;
        ensure_equals("std::array char[] trace failed", test_output(), td);
    }
    //--------------------------------------------------------------------------------------------------------------------------
    // std::array unsigned int

    template <>
    template <>
    void trace_test::test<3>() {
        set_test_name("std::array int output");
        std::array<uint16_t, 5> test_data { 0xDEAD, 0xFEED, 0xCEED, 0xCAB, 0xBAD };
        std::string td { "  0xdead  0xfeed  0xceed   0xcab   0xbad" };

        tstrm << std::hex << std::showbase << std::setw(8) << test_data;
        mys::terr << code_line << test_output() << mys::nl;
        ensure_equals("std::array unsigned int trace failed", test_output(), td);
    }
    //--------------------------------------------------------------------------------------------------------------------------
    // std::array int

    template <>
    template <>
    void trace_test::test<4>() {
        set_test_name("std::array int output");
        std::array<int, 5> test_data { INT_MAX, INT_MIN, 1, 0, -1 };
        std::string td { "  2147483647 -2147483648           1           0          -1" };

        tstrm << std::setw(12) << test_data;
        mys::terr << code_line << test_output() << mys::nl;
        ensure_equals("std::array char[] trace failed", test_output(), td);
    }
    //--------------------------------------------------------------------------------------------------------------------------
    // std::string

    template <>
    template <>
    void trace_test::test<5>() {
        set_test_name("std::string output");

        std::string test_data { "Hello World Test" };

        tstrm << test_data;
        mys::terr << code_line << test_output() << mys::nl;
        ensure_equals("std::string trace failed", test_output(), test_data);
    }
    //--------------------------------------------------------------------------------------------------------------------------
    // string with unsigned char

    using ustring = std::basic_string<uint8_t>;

    template <>
    template <>
    void trace_test::test<6>() {
        set_test_name("string unsigned char output");

        ustring test_data { 0x20, 0x1 };
        std::string td { " 0x20 0x01" };
        tstrm << std::hex << std::showbase << std::setw(4) << test_data;
        mys::terr << code_line << test_output() << mys::nl;
        ensure_equals("std::string trace failed", test_output(), td);
    }
    //--------------------------------------------------------------------------------------------------------------------------
    // std::vector

    template <>
    template <>
    void trace_test::test<7>() {
        set_test_name("std::vector output");

        std::vector test_data { 32, 33 };
        std::string td { " 32 33" };

        tstrm << std::setw(3) << test_data;
        mys::terr << code_line << test_output() << mys::nl;
        ensure_equals("std::vector trace failed", test_output(), td);
    }
    //=====================================================================================================================
    // char*
    //
    template <>
    template <>
    void trace_test::test<12>() {
        set_test_name("char* / int8_t* output");

        char const* test_data { "Hello World" };
        tstrm << test_data;
        mys::terr << code_line << test_output() << mys::nl;
        ensure_equals("char* trace failed", test_output(), test_data);
    }
    //--------------------------------------------------------------------------------------------------------------------------
    // char[]
    //
    template <>
    template <>
    void trace_test::test<13>() {
        set_test_name("char[] / int8_t[] output");

        char test_data[] { "Hello World" };
        tstrm << test_data;
        mys::terr << code_line << test_output() << mys::nl;
        ensure_equals("char[] trace failed", test_output(), test_data);
    }
    //--------------------------------------------------------------------------------------------------------------------------
    // uint8_t[] as hex

    template <>
    template <>
    void trace_test::test<14>() {
        set_test_name("uint8_t[] output");

        uint8_t test_data[] { 0x40, 0x41, 0x42, 0x01 };

        std::string td(" 0x40 0x41 0x42 0x01");

        tstrm << std::hex << std::showbase << std::setw(4) << test_data;
        mys::terr << code_line << test_output() << mys::nl;
        ensure_equals("uint8_t[] trace failed", test_output(), td);
    }

}    // namespace end

/*
 * TraceBase.cpp
 *
 *  Created on: Jul 29, 2018
 *      Author: rmerriam
 */
#include <tut/tut.hpp>

#include "Trace.h"

using ustring = std::basic_string<uint8_t>;
//--------------------------------------------------------------------------------------------------------------------------
struct trace_test_data {
    std::stringstream str_out;

    std::osyncstream sync_out { str_out };

    mys::Trace tstrm { sync_out, 'S' };

    //--------------------------------------------------------------------------------------------------------------------------
    std::string test_output() {
        tstrm.flush();
        sync_out.emit();
        return str_out.rdbuf()->str().substr(22);
    }
};
//=====================================================================================================================
namespace tut {

    typedef test_group<trace_test_data> tg;
    tg xlp_name("Trace Output POD");

    typedef tg::object trace_test;
    //=====================================================================================================================
    // bool

    template <>
    template <>
    void trace_test::test<1>() {
        set_test_name("bool output");
        std::string td("false");

        bool const test_data { false };

        tstrm << test_data;
        mys::ttmp << code_line << test_output() << mys::nl;
        ensure_equals("bool false trace failed", test_output(), td);
    }
    //--------------------------------------------------------------------------------------------------------------------------
    // bool

    template <>
    template <>
    void trace_test::test<2>() {
        set_test_name("bool output");
        std::string td("true");

        bool const test_data { true };

        tstrm << test_data;
        mys::ttmp << code_line << test_output() << mys::nl;
        ensure_equals("bool true trace failed", test_output(), td);
    }
    //--------------------------------------------------------------------------------------------------------------------------
    // char

    template <>
    template <>
    void trace_test::test<3>() {
        set_test_name("char output");
        {
            std::string td("A");
            char const test_data { 'A' };

            tstrm << test_data;
            mys::ttmp << code_line << test_output() << mys::nl;
            ensure_equals("char trace failed", test_output(), td);
        }
        {
            str_out.str(std::string());

            std::string td("0");
            char const test_data { 0x0 };

            tstrm << test_data;
            mys::ttmp << code_line << test_output() << mys::nl;
            ensure_equals("char 0 trace failed", test_output(), td);
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------
    // uchar / uint8_t

    template <>
    template <>
    void trace_test::test<4>() {
        set_test_name("uint8_t output");
        std::string td(" 0x42");

        uint8_t const test_data { 0x42 };

        tstrm << std::hex << std::showbase << test_data;
        mys::ttmp << code_line << test_output() << mys::nl;
        ensure_equals("u_char / uint8_t trace failed", test_output(), td);
    }
    //--------------------------------------------------------------------------------------------------------------------------
    // int16_t

    template <>
    template <>
    void trace_test::test<5>() {
        set_test_name("int16_t output");
        std::string td("1234");

        int16_t const test_data { 1234 };

        tstrm << test_data;
        mys::ttmp << code_line << test_output() << mys::nl;
        ensure_equals("int16_t trace failed", test_output(), td);
    }
    //--------------------------------------------------------------------------------------------------------------------------
    // uint16_t

    template <>
    template <>
    void trace_test::test<6>() {
        set_test_name("uint16_t output");
        std::string td("0x1234");

        uint16_t const test_data { 0x1234 };

        tstrm << std::hex << std::showbase << test_data;
        mys::ttmp << code_line << test_output() << mys::nl;
        ensure_equals("uint16_t trace failed", test_output(), td);
    }
    //--------------------------------------------------------------------------------------------------------------------------
    // float
    template <>
    template <>
    void trace_test::test<7>() {
        set_test_name("float output");
        std::string td("12.340");

        float const test_data { 12.34f };

        tstrm << test_data;
        mys::ttmp << code_line << test_output() << mys::nl;
        ensure_equals("float trace failed", test_output(), td);
    }
    //--------------------------------------------------------------------------------------------------------------------------
    // double
    template <>
    template <>
    void trace_test::test<8>() {
        set_test_name("double output");
        std::string td("12.340");

        double const test_data { 12.34 };

        tstrm << test_data;
        mys::ttmp << code_line << test_output() << mys::nl;
        ensure_equals("double trace failed", test_output(), td);
    }
}    // namespace end

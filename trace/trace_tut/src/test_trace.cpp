/*
 * tut_bt.cpp
 *
 *  Created on: Jul 29, 2018
 *      Author: rmerriam
 */

#include <tut/tut.hpp>
#include <tut/tut_reporter.hpp>

#include "Trace.h"

namespace tut {
    test_runner_singleton runner;
}
//--------------------------------------------------------------------------------------------------------------------------
int main( ) {

    tut::reporter reporter;
    tut::runner.get( ).set_callback(&reporter);

    tut::test_result tr;

    //mys::TraceOff t_off(mys::tout);
    tut::runner.get( ).run_tests("Trace Output POD");
    tut::runner.get( ).run_tests("Trace Output Containers");

    return !reporter.all_ok( );
}

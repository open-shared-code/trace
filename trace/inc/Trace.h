#pragma once
//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//		 File: Trace.h
//
//     Author: rmerriam
//
//    Created: Jul 3, 2021
//
//======================================================================================================================
#include <algorithm>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <ranges>
#include <sstream>
#include <syncstream>
//---------------------------------------------------------------------------------------------------------------------
namespace mys {

#include "TraceBase.h"
#include "TraceOnOff.h"
//---------------------------------------------------------------------------------------------------------------------
    constexpr char tab { '\t' };
    constexpr char nl { '\n' };
    constexpr char sp { ' ' };
    constexpr char comma[] { ", " };
//---------------------------------------------------------------------------------------------------------------------
    class Trace : public TraceBase {
    public:
        Trace(std::ostream& os, char const ch) :
            TraceBase { os, ch } {
        }

        Trace(TraceBase const& other) = delete;
        Trace(TraceBase&& other) = delete;
        Trace& operator=(TraceBase const& other) = delete;

        template <typename T>
        TraceBase& operator<<(T const& value);

        void flush() {
            mOs.emit();
        }

    private:
        void time_stamp();
    };
//---------------------------------------------------------------------------------------------------------------------
    template <typename T>
    inline TraceBase& Trace::operator<<(T const& value) {
        if (mEnabled) {
            mHex = mOs.flags() & std::ios_base::hex;
            mWidth = mOs.width();

            mOs << std::endl;
            flush();
            emit(mLeadChar);
            emit(',');
            time_stamp();
            emit(value);
        }
        return mTrace;
    }
//---------------------------------------------------------------------------------------------------------------------
    inline void Trace::time_stamp() {
        if (mEnabled) {
            using namespace ::std::chrono;
            time_point now = system_clock::now();
            std::time_t timer = system_clock::to_time_t(now);

            mOs << std::dec;
            emit(std::put_time(std::localtime( &timer), "%y%m%d,%H%M%S,"));
            mOs << std::setfill('0');
            mOs << std::setw(3);
            mOs << std::dec;
//        emit((duration_cast<milliseconds>(now.time_since_epoch()) % 1000).count());
            mOs << (duration_cast<milliseconds>(now.time_since_epoch()) % 1000).count();
            emit(": ");
        }
    }
    /*=====================================================================================================================
     * Outputs to std::cout which control what TraceBase statements are output
     *  All outputs are on by default but can be selectively turned off globally
     *  and enabled locally... or turned on globally and off locally
     *
     *      mys::TraceBaseOff dbg_off { mys::tdbg };    // turn off tdbg (debug)
     *      mys::TraceBaseOn dbg_o { mys::tdbg };       // turn of tdbg (debug)
     *
     *  These are object creations so their destructors are called when the containing
     *  block is left which reverses their action. This is regardless of the global
     *  setting, i.e. global off will be reversed when TraceBaseOff leaves a block.
     *
     *  TODO: look at global control.
     *
     */
    inline Trace terr { ::std::cerr, 'E' };
    inline Trace ttmp { ::std::cout, 'T' };    // for temporary use. different so easily found for removal
    inline Trace tlog { ::std::clog, 'L' };
    inline Trace tout { ::std::cout, 'O' };

/*=====================================================================================================================
 *  Some utility defines. Unfortunately through C++17 can't get function names, etc
 *  Location info is available in C++20 so that's a TODO
 *
 *  code_line: display function and file line number TraceBase statements
 *  code_enty: display function and 'entry'
 *  code_exit: display function and 'exit' for return at end of function
 *  code_return: display function and 'return' for an early return from function
 */

}    // namespace

#define code_line __FUNCTION__ << mys::sp << __LINE__ << mys::tab
#define code_entry __FUNCTION__ << " entry" << mys::tab
#define code_exit __FUNCTION__ << " exit" << mys::tab
#define code_return __FUNCTION__ << " return" << mys::tab
